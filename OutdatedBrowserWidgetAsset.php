<?php
/**
 * @project     Yii2 Outdated Browser Widget
 * @filename    OutdatedBrowserWidgetAsset.php
 * @author      Mirdani Handoko <mirdani.handoko@gmail.com>
 * @copyright   copyright (c) 2015, Mirdani Handoko
 * @license     BSD-3-Clause
 */

namespace mdscomp;

use Yii;
use yii\web\AssetBundle;

class OutdatedBrowserWidgetAsset extends AssetBundle {
	public $js      = ['outdatedbrowser/outdatedbrowser.min.js'];
	public $css     = ['outdatedbrowser/outdatedbrowser.min.css'];
	public $depends = [
		'yii\web\JqueryAsset'
	];

	public function init() {
		$this->sourcePath = '@bower/outdated-browser';
		parent::init();
	}
}
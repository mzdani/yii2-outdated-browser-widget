Yii2 Outdated Browser
=============
Yii2 widget for [Outdated Browser JS](http://outdatedbrowser.com/).

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist "mdscomp/yii2-outdated-browser-widget" "dev-master"
```

or add

```
"mdscomp/yii2-outdated-browser-widget": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

[Usage Wiki](https://bitbucket.org/lietsen/mdscomp-yii2-outdated-browser-wisget/wiki/Usage)